//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2013 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

require([
		"dojo/_base/event",
		"dojo/_base/lang",
		"dojo/dom-class",
		"dojo/dom-style",
		"dojo/has",
		"dojo/on",
		"dojo/query",
		"dojo/_base/sniff",
		"dojo/domReady!",
		"dojo/NodeList-dom",
		"dojo/NodeList-traverse"
	], function(event, lang, domClass, domStyle, has, on, query) {
	var active = {};
	var ajaxRefresh = "";
	activate = function(target) {
		if(this.ajaxRefresh == "true"){
			setAjaxRefresh(""); // No more refresh till shopper leaves this page
			// Update the Context, so that widget gets refreshed..
			wc.render.updateContext("departmentSubMenuContext", {"targetId":target.id});
			return;
		}

		var parent = target.getAttribute("data-parent");
		if (parent && active[parent]) {
			deactivate(active[parent]);
		}
		if (parent) {
			activate(document.getElementById(parent));
		}
		domClass.add(target, "active");
		query("a[data-activate='" + target.id + "']").addClass("selected");
		query("a[data-toggle='" + target.id + "']").addClass("selected");
		active[parent] = target;
	};
	deactivate = function(target) {
		if (active[target.id]) {
			deactivate(active[target.id]);
		}
		domClass.remove(target, "active");
		query("a[data-activate='" + target.id + "']").removeClass("selected");
		query("a[data-toggle='" + target.id + "']").removeClass("selected");
		var parent = target.getAttribute("data-parent");
		delete active[parent];
	};
	toggle = function(target) {
		if (domClass.contains(target, "active")) {
			deactivate(target);
		}
		else {
			activate(target);
		}
	};

	setUpEventActions = function(){
		query("a[data-activate]").on("click", function(e) {
			var target = this.getAttribute("data-activate");
			activate(document.getElementById(target));
			event.stop(e);
		});
		query("a[data-deactivate]").on("click", function(e) {
			var target = this.getAttribute("data-deactivate");
			deactivate(document.getElementById(target));
			event.stop(e);
		});
		query("a[data-toggle]").on("click", function(e) {
			var target = this.getAttribute("data-toggle");
			toggle(document.getElementById(target));
			event.stop(e);
		});
		query("a[data-toggle]").on("keydown", function(e) {
			if (e.keyCode == 27) {
				var target = this.getAttribute("data-toggle");
				deactivate(document.getElementById(target));
				event.stop(e);
			}
		});
		var departmentsButton = document.getElementById("departmentsButton");
		var departmentsMenu = document.getElementById("departmentsMenu");
		var departmentButtons = query(".departmentButton");
		var departmentMenus = query(".departmentMenu");
		var departmentLinks = query(".departmentMenu > h3 > .link");
		var departmentToggles = query(".departmentMenu > h3 > .toggle");
		var allDepartmentsButton = document.getElementById("allDepartmentsButton");
		var allDepartmentsMenu = document.getElementById("allDepartmentsMenu");
		var allDepartmentsMenuItems = query("#allDepartmentsMenu > li > a");
		on(departmentsButton, "keydown", function(e) {
			if (e.keyCode == 40) {
				activate(departmentsMenu);
				departmentLinks[0].focus();
				event.stop(e);
			}
		});
		departmentButtons.forEach(function(departmentButton, i) {
			on(departmentButton, "keydown", function(e) {
				switch (e.keyCode) {
				case 37:
					departmentButtons[i == 0 ? departmentButtons.length - 1 : i - 1].focus();
					if (domClass.contains(this, "active")) {
						activate(departmentMenus[i == 0 ? departmentMenus.length - 1 : i - 1]);
					}
					event.stop(e);
					break;
				case 39:
					departmentButtons[(i + 1) % departmentButtons.length].focus();
					if (domClass.contains(this, "active")) {
						activate(departmentMenus[(i + 1) % departmentMenus.length]);
					}
					event.stop(e);
					break;
				case 40:
					activate(departmentMenus[i]);
					if (this == allDepartmentsButton) {
						allDepartmentsMenuItems[0].focus();
					}
					else {
						departmentLinks[i].focus();
					}
					event.stop(e);
					break;
				}
			});
		});
		departmentMenus.forEach(function(departmentMenu, i) {
			on(departmentMenu, "keydown", function(e) {
				switch (e.keyCode) {
				case 37:
					departmentButtons[i == 0 ? departmentButtons.length - 1 : i - 1].focus();
					if (domClass.contains(this, "active")) {
						activate(departmentMenus[i == 0 ? departmentMenus.length - 1 : i - 1]);
					}
					event.stop(e);
					break;
				case 39:
					departmentButtons[(i + 1) % departmentButtons.length].focus();
					if (domClass.contains(this, "active")) {
						activate(departmentMenus[(i + 1) % departmentMenus.length]);
					}
					event.stop(e);
					break;
				}
			});
		});
		departmentMenus.forEach(function(departmentMenu, i) {
			on(departmentMenu, "keydown", function(e) {
				switch (e.keyCode) {
				case 37:
					departmentButtons[i == 0 ? departmentButtons.length - 1 : i - 1].focus();
					if (domClass.contains(this, "active")) {
						activate(departmentMenus[i == 0 ? departmentMenus.length - 1 : i - 1]);
					}
					event.stop(e);
					break;
				case 39:
					departmentButtons[(i + 1) % departmentButtons.length].focus();
					if (domClass.contains(this, "active")) {
						activate(departmentMenus[(i + 1) % departmentMenus.length]);
					}
					event.stop(e);
					break;
				}
			});
		});
		on(allDepartmentsMenu, "keydown", function(e) {
			if (e.keyCode == 27) {
				deactivate(allDepartmentsMenu);
				allDepartmentsButton.focus();
				event.stop(e);
			}
		});
		allDepartmentsMenuItems.forEach(function(allDepartmentsMenuItem, i) {
			on(allDepartmentsMenuItem, "keydown", function(e) {
				switch (e.keyCode) {
				case 38:
					allDepartmentsMenuItems[i == 0 ? allDepartmentsMenuItems.length - 1 : i - 1].focus();
					event.stop(e);
					break;
				case 40:
					allDepartmentsMenuItems[(i + 1) % allDepartmentsMenuItems.length].focus();
					event.stop(e);
					break;
				}
			});
		});
		if (has("ie") < 10) {
			query("input[placeholder]").forEach(function(input) {
				var placeholder = input.getAttribute("placeholder");
				if (placeholder) {
					var label = document.createElement("label");
					label.className = "placeholder";
					label.innerHTML = placeholder;
					input.parentNode.insertBefore(label, input);
					var updatePlaceholder = function() {
						label.style.display = (input.value ? "none" : "block");
					};
					window.setTimeout(updatePlaceholder, 200);
					on(input, "blur, focus, keyup", updatePlaceholder);
				}
			});
		}
	};

	setUpEventActions();

	window.setTimeout(function() {
			var quickLinksBar = document.getElementById("quickLinksBar");
			var quickLinksButton = document.getElementById("quickLinksButton");
			var quickLinksMenu = document.getElementById("quickLinksMenu");
			var quickLinksMenuItems = query("> ul > li > a", quickLinksMenu);
			query("#quickLinksMenu > ul > li").forEach(function(li) {
				li = li.cloneNode(true);
				query("[id]", li).forEach(function(node) {
					node.id += "_alt";
				});
				quickLinksBar.insertBefore(li, quickLinksBar.firstChild);
			});
			query("#quickLinksBar > li > a, #quickLinksBar > li > span").forEach(function(node) {
				if (node.id != "miniCartButton") {
					var s = lang.trim(node.innerHTML);
					var n = s.lastIndexOf(" ");
					if (n != -1) {
						//node.innerHTML = s.substring(0, n + 1) + "<br/>" + s.substring(n + 1, s.length);
					}
				}
			});
			on(quickLinksButton, "keydown", function(e) {
				if (e.keyCode == 40) {
					activate(quickLinksMenu);
					quickLinksMenuItems[0].focus();
					event.stop(e);
				}
			});
			on(quickLinksMenu, "keydown", function(e) {
				if (e.keyCode == 27) {
					deactivate(quickLinksMenu);
					quickLinksButton.focus();
					event.stop(e);
					
				}
			});
			quickLinksMenuItems.forEach(function(quickLinksMenuItem, i) {
				quickLinksMenuItem.setAttribute("role", "menuitem");
				quickLinksMenuItem.setAttribute("tabIndex", "-1");
				on(quickLinksMenuItem, "keydown", function(e) {
					switch (e.keyCode) {
					case 38:
						quickLinksMenuItems[i == 0 ? quickLinksMenuItems.length - 1 : i - 1].focus();
						event.stop(e);
						break;
					case 40:
						quickLinksMenuItems[(i + 1) % quickLinksMenuItems.length].focus();
						event.stop(e);
						break;
					}
				});
			});
		}, 100);

	var header = document.getElementById("header");
	var direction = domStyle.getComputedStyle(header).direction;
	
	updateQuickLinksBar = function() {
		var logo = document.getElementById("logo");
		var quickLinksBar = document.getElementById("quickLinksBar");
		var availableWidth = (direction == "rtl" ? logo.offsetLeft - quickLinksBar.offsetLeft : (quickLinksBar.offsetLeft + quickLinksBar.offsetWidth) - (logo.offsetLeft + logo.offsetWidth));
		var quickLinksBarItems = query("#quickLinksBar > li");
		var quickLinksItem = quickLinksBarItems[quickLinksBarItems.length - 2];
		var miniCartItem = quickLinksBarItems[quickLinksBarItems.length - 1];
		availableWidth -= quickLinksItem.offsetWidth + miniCartItem.offsetWidth;
		for (var i = quickLinksBarItems.length - 3; i >= 0; i--) {
			availableWidth -= quickLinksBarItems[i].offsetWidth;
			domClass.toggle(quickLinksBarItems[i], "border-right", (availableWidth >= 0));
			domClass.toggle(quickLinksBarItems[i], "hidden", (availableWidth < 0));
		}
	};
	window.setTimeout(updateQuickLinksBar, 200);
	on(window, "resize", updateQuickLinksBar);
	
	updateDepartmentsMenu = function() {
		var departmentsMenu = document.getElementById("departmentsMenu");
		var searchBar = document.getElementById("searchBar");
		var departmentsMenuItems = query("#departmentsMenu > li");
		var allDepartmentsItem = departmentsMenuItems[departmentsMenuItems.length - 1];
		availableWidth = (direction == "rtl" ? (departmentsMenu.offsetLeft + departmentsMenu.offsetWidth) - (searchBar.offsetLeft + searchBar.offsetWidth) : searchBar.offsetLeft - departmentsMenu.offsetLeft) - allDepartmentsItem.offsetWidth;
		for (var i = 0; i < departmentsMenuItems.length - 1; i++) {
			availableWidth -= departmentsMenuItems[i].offsetWidth;
			domClass.toggle(departmentsMenuItems[i], "hidden", (availableWidth < 0));
		}
	};
	window.setTimeout(updateDepartmentsMenu, 200);
	if (!(has("ie") < 9)) { // Disabled due to an IE8 bug causing the page to go partially black
		on(window, "resize", updateDepartmentsMenu);
	}
	
	query("#searchFilterMenu > ul > li > a").on("click", function(e) {
		document.getElementById("searchFilterButton").innerHTML = this.innerHTML;
		document.getElementById("categoryId").value = this.getAttribute("data-value");
		deactivate(document.getElementById("searchFilterMenu"));
	});
	query("#searchBox > .submitButton").on("click", function(e) {
		document.getElementById("searchBox").submit();
	});

	// Context and Controller to refresh department drop-down
	wc.render.declareContext("departmentSubMenuContext",{targetId: ""},"");
	wc.render.declareRefreshController({
	id: "departmentSubMenu_Controller",
	renderContext: wc.render.getContextById("departmentSubMenuContext"),
	url: "",
	formId: "",
	
	renderContextChangedHandler: function(message, widget) {
		cursor_wait();
		widget.refresh(this.renderContext.properties);
	},
		   
	postRefreshHandler: function(widget) {
		 setUpEventActions();  // Again setup the events, since entire department listing HTML elements changed
		 updateDepartmentsMenu(); // Browser may be re-sized. From server we return entire department list.. updateHeader to fit to the list within available size
 		 activate(document.getElementById( this.renderContext.properties.targetId)); // We have all the data.. Activate the menu...
   		 cursor_clear();
	} 
	});

	setAjaxRefresh = function(refresh){
		this.ajaxRefresh = refresh;
	}

});
//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2013 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

/**
 *  Scroll between set of product/category recommendations
 */	
var scrollCarousel = function(carousel, index, wrap) {
	// Workaround before we completely replace this with the Dojo widget
	if (carousel.getAttribute("data-dojo-type") == "wc/widget/Carousel") {
		return;
	}
	var oldIndex = parseInt(carousel.getAttribute("data-index"));
	if (index == null) {
		index = oldIndex;

	}
	var content = carousel.querySelector(".category");
	var ul = carousel.querySelector("ul");
	var curDir = ul.currentStyle? ul.currentStyle.direction : window.getComputedStyle(ul, null).direction;
	var ltr = curDir != "rtl";
	var lis = carousel.querySelectorAll(".category > ul > li");
	var main = false;
	ul.style.overflow = "hidden"; // FF workaround
	
	if(ul.className=="homeHeroSpot"){
		document.getElementById("mainESpotHome").className = "closed";
		var content = carousel;
		var lis = carousel.querySelectorAll("ul > li > div");
		main = true;
		for (var i = 0; i < lis.length; i++) {
			if (!ltr && lis[i].style.marginLeft != "0%"){
				lis[i].style.visibility = "hidden";
				lis[i].style.marginRight = lis[i].style.marginLeft;
				lis[i].style.marginLeft = "0%";
			}
			lis[i].querySelector(".productDescDrop").setAttribute("aria-expanded", "false");
		}
		
		/* Fadein background image */	
		var homeEspotBackgroundElements = document.getElementsByTagName('*'),i;
		for (i in homeEspotBackgroundElements) {
			if((" "+homeEspotBackgroundElements[i].className+" ").indexOf("homePageBackground") > -1 && index < lis.length) {
				homeEspotBackgroundElements[i].style.cssText = "opacity: 0;filter: alpha(opacity=0);-moz-opacity: 0;";
				homeEspotBackgroundElements[i].className = "eSpotContainer hidden homePageBackground";
			}
		}
		if (index > -1 && index < lis.length) {
			document.getElementById ("background" + (index+1)).style.cssText = "opacity: 0.75;filter: alpha(opacity=75);-moz-opacity: 0.75;";
			document.getElementById ("background" + (index+1)).className = "eSpotContainer shown homePageBackground";
		}
	}
	
	if (index) {
		var count = Math.ceil(ul.scrollWidth / content.clientWidth);
		if (wrap) {
			index %= count;
			if (index < 0) { index += count; }
		}
		else {
			index = Math.max(0, Math.min(index, Math.min(count - 1, lis.length-1)));
		}
	}
	carousel.setAttribute("data-index", index.toString());
	
	ul.style.overflow = "visible"; // FF workaround
	if (ltr) {
		ul.style.left = (-index * 100) + "%";
		ul.style.transition = "left .5s";
		ul.style.mozTransition = "left .5s";
		ul.style.webkitTransition = "left .5s";
	}
	else {
		ul.style.right = (-index * 100) + "%";
		ul.style.transition = "right .5s";
		ul.style.mozTransition = "right .5s";
		ul.style.webkitTransition = "right .5s";
	}
	for (var i = 0; i < lis.length; i++) {
		var visible = (main? lis[i].offsetLeft / content.clientWidth :  
				Math.floor((lis[i].offsetLeft + (ltr? 0 : lis[i].offsetWidth)) / content.clientWidth)) == (ltr ? index : -index);
		lis[i].setAttribute("aria-hidden", visible ? "false" : "true");
	}
	window.setTimeout(function() {
		for (var i = 0; i < lis.length; i++) {
			var visible = (main? lis[i].offsetLeft / content.clientWidth : 
				Math.floor((lis[i].offsetLeft + (ltr? 0 : lis[i].offsetWidth)) / content.clientWidth)) == (ltr ? index : -index);
			lis[i].style.visibility = visible ? "visible" : "hidden";
		}
	}, 500);
}

require(["dojo/on", "dojo/query", "dojo/NodeList-traverse", "dojo/domReady!"], function(on, query) {
	var carousels = document.querySelectorAll(".carousel");
	for (var i = 0; i < carousels.length; i++) {
		scrollCarousel(carousels[i], null, false);
	}
	on(document, ".carousel .navPrev:click", function(event) {
		(event.preventDefault) ? event.preventDefault() : event.returnValue = false;
		var carousel = query(this).parents(".carousel")[0];
		var index = parseInt(carousel.getAttribute("data-index"));
		scrollCarousel(carousel, index - 1, true);
	});
	on(document, ".selecteSpot .dot:click", function(event) {
		(event.preventDefault) ? event.preventDefault() : event.returnValue = false;
	});
	on(document, ".carousel .navNext:click", function(event) {
		(event.preventDefault) ? event.preventDefault() : event.returnValue = false;
		var carousel = query(this).parents(".carousel")[0];
		var index = parseInt(carousel.getAttribute("data-index"));
		scrollCarousel(carousel, index + 1, true);
	});
	on(window, "resize", function(event) {
		query(".carousel").forEach(function(carousel, i) {
			scrollCarousel(carousel, 0, false);
		});
	});
	on(document, ".carousel:touchstart", function(event) {
		// Workaround before we completely replace this with the Dojo widget
		if (this.getAttribute("data-dojo-type") == "wc/widget/Carousel") {
			return;
		}
		if (event.touches.length != 1) {
			// Not single-touch
			return;
		}
		var index = parseInt(this.getAttribute("data-index"));
		this.swipe = {
			startX: event.touches[0].pageX,
			startY: event.touches[0].pageY,
			translateX: -this.clientWidth * index,
			deltaX: 0,
			startTime: new Date().getTime()
		};
		this.swipe.ul = this.querySelector("ul");
		this.swipe.ul.style.transition = "none";
		this.swipe.ul.style.mozTransition = "none";
		this.swipe.ul.style.webkitTransition = "none";
		var lis = this.querySelectorAll(".category > ul > li");
		var curDir = this.swipe.ul.currentStyle? this.swipe.ul.currentStyle.direction : window.getComputedStyle(this.swipe.ul, null).direction;
		this.swipe.ltr = curDir != "rtl";		
		for (var i = 0; i < lis.length; i++) {
			lis[i].style.visibility = "visible";
		}
	});
	on(document, ".carousel:touchmove", function(event) {
		if (this.swipe) {
			if (event.touches.length != 1) {
				// Not single-touch
				scrollCarousel(this, null, false);
				return;
			}
			this.swipe.deltaX = event.touches[0].pageX - this.swipe.startX;
			if (this.swipe.startY) {
				var deltaY = event.touches[0].pageY - this.swipe.startY;
				if (Math.abs(deltaY) > Math.abs(this.swipe.deltaX)) {
					// Not horizontal swipe
					scrollCarousel(this, null, false);
					return;
				}
				// Stop further checks
				delete this.swipe.startY;
			}
			if (this.swipe.ltr) {
				this.swipe.ul.style.left = (this.swipe.translateX + this.swipe.deltaX)*100/this.clientWidth + "%";
			}
			else {
				this.swipe.ul.style.right = -((-this.swipe.translateX + this.swipe.deltaX)*100/this.clientWidth) + "%";
			}
			
			event.preventDefault();
		}
	});
	on(document, ".carousel:touchend", function(event) {

		if (this.swipe) {
			var absDeltaX = Math.abs(this.swipe.deltaX);
			var deltaT = new Date().getTime() - this.swipe.startTime;
			if (absDeltaX < 20 && deltaT < 200) {
				// Not swipe
				scrollCarousel(this, null, false);
				return;
			}
			var index = parseInt(this.getAttribute("data-index"));
			if (absDeltaX >= 20 && deltaT < 200 || absDeltaX >= this.clientWidth / 2) {
				if (this.swipe.ltr) {
					index += this.swipe.deltaX > 0 ? -1 : 1;
				}
				else {
					index += this.swipe.deltaX > 0 ? 1 : -1;
				}
			}
			scrollCarousel(this, index, false);
			event.preventDefault();
		}
	});
});

/**
 *  Swipe between main Herospots on Home Page & tap 
 *	call carousel to handle swipe gestures on herospot
 */	
  function rotateHomeESpot() {
	var homePageCarousel = document.getElementById ("homePageCarousel");
	homePageCarousel.setAttribute("data-index", arguments [0] - 1);
	scrollCarousel(homePageCarousel, null, false);
  }

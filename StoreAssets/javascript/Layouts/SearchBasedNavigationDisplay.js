//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2011, 2013 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

// Declare context and refresh controller which are used in pagination controls of SearchBasedNavigationDisplay -- both products and articles+videos
wc.render.declareContext("searchBasedNavigation_context", {"contentBeginIndex":"0", "productBeginIndex":"0", "beginIndex":"0", "orderBy":"", "facetId":"", "pageView":"", "resultType":"both", "orderByContent":"", "searchTerm":"", "facet":"", "facetLimit":"", "minPrice":"", "maxPrice":""}, "");

// Declare context and refresh controller which are used in pagination controls of SearchBasedNavigationDisplay to display content results (Products).
wc.render.declareRefreshController({
	id: "searchBasedNavigation_controller",
	renderContext: wc.render.getContextById("searchBasedNavigation_context"),
	url: "",
	formId: ""

,renderContextChangedHandler: function(message, widget) {
	console.log("renderContextChangedHandler");
	var controller = this;
	var renderContext = this.renderContext;
	var resultType = renderContext.properties["resultType"];
	if(resultType == "products" || resultType == "both"){
		renderContext.properties["beginIndex"] = renderContext.properties["productBeginIndex"];
		widget.refresh(renderContext.properties);
	}
	
}

,postRefreshHandler: function(widget) {
	console.log("postRefreshHandler");
	// Handle the new facet counts, and update the values in the left navigation.  First parse the script, and then call the update function.
	var facetCounts = byId("facetCounts");
	if(facetCounts != null) {
		var scripts = facetCounts.getElementsByTagName("script");
		var j = scripts.length;
		for (var i = 0; i < j; i++){
			var newScript = document.createElement('script');
			newScript.type = "text/javascript";
			newScript.text = scripts[i].text;
			facetCounts.appendChild(newScript);
		}
		SearchBasedNavigationDisplayJS.resetFacetCounts();
		updateFacetCounts();
		//uncomment this if you want tohide zero facet values and the facet itself
		//SearchBasedNavigationDisplayJS.removeZeroFacetValues();
		SearchBasedNavigationDisplayJS.validatePriceInput();
	}

	var resultType = widget.controller.renderContext.properties["resultType"];
	if(resultType == "products" || resultType == "both"){
		var currentIdValue = currentId;
		cursor_clear();  
		SearchBasedNavigationDisplayJS.initControlsOnPage(widget.controller.renderContext.properties);
		shoppingActionsJS.updateSwatchListView();
		shoppingActionsJS.checkForCompare();
		var gridViewLinkId = "WC_SearchBasedNavigationResults_pagination_link_grid_categoryResults";
		var listViewLinkId = "WC_SearchBasedNavigationResults_pagination_link_list_categoryResults";
		if(currentIdValue == "orderBy"){
			byId("orderBy").focus();
		}
		else{
			if((currentIdValue == gridViewLinkId || currentIdValue != listViewLinkId) && byId(listViewLinkId)){
				byId(listViewLinkId).focus();
			}
			if((currentIdValue == listViewLinkId || currentIdValue != gridViewLinkId) && byId(gridViewLinkId)){
				byId(gridViewLinkId).focus();
			}
		}
	}
	var pagesList = document.getElementById("pages_list_id");
	if (pagesList != null && !isAndroid() && !isIOS()) {
		dojo.addClass(pagesList, "desktop");
	}
	dojo.publish("CMPageRefreshEvent");
}
});

// Declare context and refresh controller which are used in pagination controls of SearchBasedNavigationDisplay to display content results (Articles and videos).
wc.render.declareRefreshController({
	id: "searchBasedNavigation_content_controller",
	renderContext: wc.render.getContextById("searchBasedNavigation_context"),
	url: "",
	formId: ""

,renderContextChangedHandler: function(message, widget) {
	var controller = this;
	var renderContext = this.renderContext;
	var resultType = renderContext.properties["resultType"];
	if(resultType == "content" || resultType == "both"){
		renderContext.properties["beginIndex"] = renderContext.properties["contentBeginIndex"];
		widget.refresh(renderContext.properties);
	}
}

,postRefreshHandler: function(widget) {
	var resultType = widget.controller.renderContext.properties["resultType"];
	if(resultType == "content" || resultType == "both"){
			var currentIdValue = currentId;
			cursor_clear();  
			SearchBasedNavigationDisplayJS.initControlsOnPage(widget.controller.renderContext.properties);
			shoppingActionsJS.initCompare();
			if(currentIdValue == "orderByContent"){
				byId("orderByContent").focus();
			}
		}
	}
});



if(typeof(SearchBasedNavigationDisplayJS) == "undefined" || SearchBasedNavigationDisplayJS == null || !SearchBasedNavigationDisplayJS){

	SearchBasedNavigationDisplayJS = {

		/** 
		 * This variable is an array to contain all of the facet ID's generated from the initial search query.  This array will be the master list when applying facet filters.
		 */

		contextValueSeparator: "&",
		contextKeySeparator: ":",
		widgetId: "",
		facetIdsArray: new Array,
		facetIdsParentArray: new Array,
		uniqueParentArray: new Array,


		init:function(searchResultUrl){
			wc.render.getRefreshControllerById('searchBasedNavigation_controller').url = searchResultUrl;
			this.initControlsOnPage(WCParamJS);
			this.updateContextProperties("searchBasedNavigation_context", WCParamJS);

//			var currentContextProperties = wc.render.getContextById('searchBasedNavigation_context').properties;
		},

		initConstants:function(removeCaption, moreMsg, lessMsg, currencySymbol) {
			this.removeCaption = removeCaption;
			this.moreMsg = moreMsg;
			this.lessMsg = lessMsg;
			this.currencySymbol = currencySymbol;
		},

		initControlsOnPage:function(properties){
			//Set state of sort by select box..
			var selectBox = dojo.byId("orderBy");
			if(selectBox != null && selectBox != 'undefined'){
				dojo.byId("orderBy").value = properties['orderBy'];
			}

			selectBox = dojo.byId("orderByContent");
			if(selectBox != null && selectBox != 'undefined'){
				dojo.byId("orderByContent").value = properties['orderByContent'];
			}
		},

		initContentUrl:function(contentUrl){
			wc.render.getRefreshControllerById('searchBasedNavigation_content_controller').url = contentUrl;
		},

		findContainer:function(el) {
		console.debug(el);
			while (el.parentNode) {
				el = el.parentNode;
				if (el.className == 'optionContainer') {
				    return el;
				}
			}
			return null;
		},

		resetFacetCounts:function() {
			for(var i = 0; i < this.facetIdsArray.length; i++) {
				var facetValue = byId("facet_count" + this.facetIdsArray[i]);
				if(facetValue != null) {
					facetValue.innerHTML = 0;
				}	
			}
		},
		removeZeroFacetValues:function() {
			var uniqueId = this.uniqueParentArray;
			var widget = this.widgetId;
			for(var i = 0; i < this.facetIdsArray.length; i++) {
				var facetId = "facet_" + this.facetIdsArray[i];
				var parentId = this.facetIdsParentArray[i];
				var facetValue = byId("facet_count" + this.facetIdsArray[i]);
				
				if(facetValue.innerHTML == '0') {
					
					document.getElementById(facetId + widget).style.display = 'none';	 
				}
				else if(facetValue.innerHTML != '0') {
					document.getElementById(facetId + widget).style.display = 'block';
					uniqueId[parentId] = uniqueId[parentId] + 1;
				}		
			}
			for(var key in uniqueId){
				if(uniqueId[key] == 0){
					document.getElementById(key).style.display = 'none';
					uniqueId[key] = 0;//reset the count
				}
				else if(uniqueId[key] != 0){
					document.getElementById(key).style.display = 'block';
					uniqueId[key] = 0;//reset the count
				}
			}
			
		},
		updateFacetCount:function(id, count, value, label, image, contextPath, group, multiFacet) {
			var facetValue = byId("facet_count" + id);
			if(facetValue != null) {
				var checkbox = byId(id);
				if(count > 0) {
					// Reenable the facet link
					checkbox.disabled = false;
					facetValue.innerHTML = count;
				}
			}	
			else if(count > 0) {
				var moresection = byId("moresection_" + group);
				if(moresection != null && moresection.style.display == "block") {
					// there is no limit to the number of facets shown, and the user has exposed the show all link
					if(byId("facet_" + id) == null) {
						// this facet does not exist in the list.  Insert it.
						var grouping = byId("more_" + group);
						if(grouping) {
							this.facetIdsArray.push(id);
							var newFacet = document.createElement("li");
							var facetClass = "";
							var section = "";
							
							if(!multiFacet) {
								facetClass = "singleFacet";
								// specify which facet group to collapse when multifacets are not enabled.
								section = group;
							}
							if(image != "") {
								facetClass = "singleFacet left";

							}

							newFacet.setAttribute("id", "facet_" + id);
							newFacet.setAttribute("class", facetClass);


							var facetLabel = "<label for='" + id + "'>";

							if(image != "") {
								facetLabel = facetLabel + "<span class='swatch'><span class='outline'><span id='facetLabel_" + id + "'><img src='" + contextPath + "/" + image + "' title='" + label + "' alt='" + label + "'/></span> (<span id='facet_count" + id + "'>" + count + "</span>)</span></span>";
							}
							else {
								facetLabel = facetLabel + "<span class='outline'><span id='facetLabel_" + id + "'>" + label + "</span> (<span id='facet_count" + id +"'>" + count + "</span>)</span>";
							}

							facetLabel = facetLabel + "<span class='spanacce' id='" + id + "_ACCE_Label'>" + label + " (" + count + ")</span></label>";

							newFacet.innerHTML = "<input type='checkbox' aria-labelledby='" + id + "_ACCE_Label' id='" + id + "' value='" + value + "' onclick='javascript: SearchBasedNavigationDisplayJS.toggleSearchFilter(this, \"" + id + "\")'/>" + facetLabel;
							grouping.appendChild(newFacet);
						}
					}
				}
			}
		},

		isValidNumber:function(n) {
			return !isNaN(parseFloat(n)) && isFinite(n) && n >= 0;
		},

		checkPriceInput:function(event) {
			if(this.validatePriceInput() && event.keyCode == 13) {
				this.appendFilterPriceRange();
				this.doSearchFilter();
			}
			return false;
		},

		validatePriceInput:function() {
			if(byId("low_price_input") != null && byId("high_price_input") != null && byId("price_range_go") != null) {
				var low = byId("low_price_input").value;
				var high = byId("high_price_input").value;
				var go = byId("price_range_go");
				if(this.isValidNumber(low) && this.isValidNumber(high) && parseFloat(high) > parseFloat(low)) {
					go.className = "go_button";
					go.disabled = false;
				}
				else {
					go.className = "go_button_disabled";
					go.disabled = true;
				}	
				return !go.disabled;
			}
			return false;
		},

		toggleShowMore:function(index, show) {
			var list = byId('more_' + index);
			var morelink = byId('morelink_' + index);
			if(list != null) {
				if(show) {
					morelink.style.display = "none";
					list.style.display = "block";
				}
				else {
					morelink.style.display = "block";
					list.style.display = "none";
				}
			}
		},
		
		toggleSearchFilterOnKeyDown:function(event, element, id) {
			if (event.keyCode == dojo.keys.ENTER) {
				element.checked = !element.checked;
				this.toggleSearchFilter(element, id);
			}
		},
		
		toggleSearchFilter:function(element, id) {

			
			if(element.checked) {
				this.appendFilterFacet(id);
			}
			else {
				this.removeFilterFacet(id);
			}

/*			

			if(section != "") {
				byId('section_' + section).style.display = "none";
			}
*/			
			this.doSearchFilter();
		},

		appendFilterPriceRange:function() {

			var el = byId("price_range_input");
			var section = this.findContainer(el);
			if(section) {
				byId(section.id).style.display = "none";
			}
			byId("clear_all_filter").style.display = "block";


			var facetFilterList = byId("facetFilterList");
			// create facet filter list if it's not exist
			if (facetFilterList == null) {
				facetFilterList = document.createElement("ul");
				facetFilterList.setAttribute("id", "facetFilterList");
				facetFilterList.setAttribute("class", "facetSelectedCont");
				var facetFilterListWrapper = byId("facetFilterListWrapper");
				facetFilterListWrapper.appendChild(facetFilterList);
			}
			
			var filter = byId("pricefilter");
			if(filter == null) {
				filter = document.createElement("li");
				filter.setAttribute("id", "pricefilter");
				filter.setAttribute("class", "facetSelected");
				facetFilterList.appendChild(filter);
			}
			var label = this.currencySymbol + byId("low_price_input").value + " - " + this.currencySymbol + byId("high_price_input").value;
			filter.innerHTML = "<a role='button' href='#' onclick='SearchBasedNavigationDisplayJS.removeFilterPriceRange(); return false;'>" + "<div class='filter_option'><div class='close'></div><span>" + label + "</span></div></a>";

			byId("clear_all_filter").style.display = "block";

			if(this.validatePriceInput()) {
				// Promote the values from the input boxes to the internal inputs for use in the request.
				byId("low_price_value").value = byId("low_price_input").value;
				byId("high_price_value").value = byId("high_price_input").value;
			}
		
		},

		removeFilterPriceRange:function() {
			if(byId("low_price_value") != null && byId("high_price_value") != null) {
				byId("low_price_value").value = "";
				byId("high_price_value").value = "";	
			}
			var facetFilterList = byId("facetFilterList");
			var filter = byId("pricefilter");
			if(filter != null) {
				facetFilterList.removeChild(filter);
			}

			if(facetFilterList.childNodes.length == 0) {
				byId("clear_all_filter").style.display = "none";
				byId("facetFilterListWrapper").innerHTML = "";
			}

			var el = byId("price_range_input");
			var section = this.findContainer(el);
			if(section) {
				byId(section.id).style.display = "block";
			}

			this.doSearchFilter();
		},

		appendFilterFacet:function(id) {
			var facetFilterList = byId("facetFilterList");
			// create facet filter list if it's not exist
			if (facetFilterList == null) {
				facetFilterList = document.createElement("ul");
				facetFilterList.setAttribute("id", "facetFilterList");
				facetFilterList.setAttribute("class", "facetSelectedCont");
				var facetFilterListWrapper = byId("facetFilterListWrapper");
				facetFilterListWrapper.appendChild(facetFilterList);
			}
			
			var filter = byId("filter_" + id);
			// do not add it again if the user clicks repeatedly
			if(filter == null) {
				filter = document.createElement("li");
				filter.setAttribute("id", "filter_" + id);
				filter.setAttribute("class", "facetSelected");
				var label = byId("facetLabel_" + id).innerHTML;

				filter.innerHTML = "<a role='button' href='#' onclick='SearchBasedNavigationDisplayJS.removeFilterFacet(\"" + id + "\"); return false;'>" + "<div class='filter_option'><div class='close'></div><span>" + label + "</span></div></a>";

				facetFilterList.appendChild(filter);
			}

			var el = byId(id);
			var section = this.findContainer(el);
			if(section) {
				byId(section.id).style.display = "none";
			}
			byId("clear_all_filter").style.display = "block";
			
		},

		removeFilterFacet:function(id) {
			
			var facetFilterList = byId("facetFilterList");
			var filter = byId("filter_" + id);
			if(filter != null) {
				facetFilterList.removeChild(filter);
				byId(id).checked = false;
			}

			if(facetFilterList.childNodes.length == 0) {
				byId("clear_all_filter").style.display = "none";
				byId("facetFilterListWrapper").innerHTML = "";
			}


			var el = byId(id);
			var section = this.findContainer(el);
			if(section) {
				byId(section.id).style.display = "block";
			}
			this.doSearchFilter();
		},

		getEnabledProductFacets:function() {
			var facetForm = document.forms['productsFacets'];
			var elementArray = facetForm.elements;

			var facetArray = new Array();
			var facetIds = new Array();
			if(_searchBasedNavigationFacetContext != 'undefined') {
				for(var i=0; i< _searchBasedNavigationFacetContext.length; i++) {
					facetArray.push(_searchBasedNavigationFacetContext[i]);
					//facetIds.push();
				}
			}
			var facetLimits = new Array();
			for (var i=0; i < elementArray.length; i++) {
				var element = elementArray[i];
				if(element.type != null && element.type.toUpperCase() == "CHECKBOX") {
					if(element.title == "MORE") {
						// scan for "See More" facet enablement.
						if(element.checked) {
							facetLimits.push(element.value);
						}
					}
					else {
						// disable the checkbox while the search is being performed to prevent double clicks
						element.disabled = true;
						if(element.checked) {
							facetArray.push(element.value);
							facetIds.push(element.id);
						}
					}
				}
			}
			// disable the price range button also
			if(byId("price_range_go") != null) {
				byId("price_range_go").disabled = true;
			}

			var results = new Array();
			results.push(facetArray);
			results.push(facetLimits);
			results.push(facetIds);
			return results;
		},

		doSearchFilter:function() {
			if(!submitRequest()){
				return;
			}
			cursor_wait();  

			var minPrice = "";
			var maxPrice = "";
			
			if(byId("low_price_value") != null && byId("high_price_value") != null) {
				minPrice = byId("low_price_value").value;
				maxPrice = byId("high_price_value").value;
			}

			var facetArray = this.getEnabledProductFacets();
			
			wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": "0", "facet": facetArray[0], "facetLimit": facetArray[1], "facetId": facetArray[2], "resultType":"products", "minPrice": minPrice, "maxPrice": maxPrice});
			this.updateHistory();
			
			MessageHelper.hideAndClearMessage();
		},

		toggleShowMore:function(element, id) {
			var section = byId("moresection_" + id);
			var label = byId("showMoreLabel_" + id);
			if(element.checked) {
				section.style.display = "block";
				label.innerHTML = this.lessMsg;
				this.doSearchFilter();
			}
			else {
				section.style.display = "none";
				label.innerHTML = this.moreMsg;
			}
		},

		clearAllFacets:function(execute) {
			byId("clear_all_filter").style.display = "none";
			byId("facetFilterListWrapper").innerHTML = "";
			if(byId("low_price_value") != null && byId("high_price_value") != null) {
				byId("low_price_value").value = "";
				byId("high_price_value").value = "";
			}

			var facetForm = document.forms['productsFacets'];
			var elementArray = facetForm.elements;
			for (var i=0; i < elementArray.length; i++) {
				var element = elementArray[i];
				if(element.type != null && element.type.toUpperCase() == "CHECKBOX" && element.checked && element.title != "MORE") {
					element.checked = false;
				}
			}

			var elems = document.getElementsByTagName("*");
			for (var i=0; i < elems.length; i++) {
				// Reset all hidden facet sections (single selection facets are hidden after one facet is selected from that facet grouping).
				var element = elems[i];
				if (element.id != null && (element.id.indexOf("section_") == 0) && !(element.id.indexOf("section_list") == 0)) {
					element.style.display = "block";
				}
			}

			if(execute) {
				this.doSearchFilter();
			}
		},

		toggleSearchContentFilter:function() {
			if(!submitRequest()){
				return;
			}
			cursor_wait();  

			var facetList = "";
			var facetForm = document.forms['contentsFacets'];
			var elementArray = facetForm.elements;
			for (var i=0; i < elementArray.length; i++) {
				var element = elementArray[i];
				if(element.type != null && element.type.toUpperCase() == "CHECKBOX" && element.checked && element.title != "MORE") {
					facetList += element.value + ";";
				}
			}
			
			wc.render.updateContext('searchBasedNavigation_context', {"facet": facetList, "resultType":"content"});
			this.updateHistory();
			MessageHelper.hideAndClearMessage();
		},


		updateContextProperties:function(contextId, properties){
			//Set the properties in context object..
			for(key in properties){
				wc.render.getContextById(contextId).properties[key] = properties[key];
				console.debug(" key = "+key +" and value ="+wc.render.getContextById(contextId).properties[key]);
			}
		},

		showResultsPageForContent:function(data){

			var pageNumber = data['pageNumber'];
			var pageSize = data['pageSize'];
			pageNumber = dojo.number.parse(pageNumber);
			pageSize = dojo.number.parse(pageSize);

			setCurrentId(data["linkId"]);

			if(!submitRequest()){
				return;
			}

			var beginIndex = pageSize * ( pageNumber - 1 );
			cursor_wait();
			wc.render.updateContext('searchBasedNavigation_context', {"contentBeginIndex": beginIndex,"resultType":"content"});
			this.updateHistory();
			MessageHelper.hideAndClearMessage();
		},

		showResultsPage:function(data){

			var pageNumber = data['pageNumber'];
			var pageSize = data['pageSize'];
			pageNumber = dojo.number.parse(pageNumber);
			pageSize = dojo.number.parse(pageSize);

			setCurrentId(data["linkId"]);

			if(!submitRequest()){
				return;
			}
			
			console.debug(wc.render.getRefreshControllerById('searchBasedNavigation_controller').renderContext.properties);
			var beginIndex = pageSize * ( pageNumber - 1 );
			cursor_wait();

			
			wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": beginIndex,"resultType":"products"});
			this.updateHistory();
			MessageHelper.hideAndClearMessage();
		},

		toggleView:function(data){
			var pageView = data["pageView"];
			setCurrentId(data["linkId"]);
			if(!submitRequest()){
				return;
			}
			cursor_wait();  
			console.debug("pageView = "+pageView+" controller = +searchBasedNavigation_controller");
			wc.render.updateContext('searchBasedNavigation_context', {"pageView": pageView,"resultType":"products"});
			this.updateHistory();
			MessageHelper.hideAndClearMessage();
		},
		
		toggleExpand:function(id) {
			var icon = byId("icon_" + id);
			var section_list = byId("section_list_" + id);
			if(icon.className == "arrow") {
				icon.className = "arrow arrow_collapsed";
				section_list.style.display = "none";
			}
			else {
				icon.className = "arrow";
				section_list.style.display = "block";
			}
		},

		setPageSize:function(newPageSize){
			if(!submitRequest()){
				return;
			}
			cursor_wait();  
			console.debug("resultsPerPage = "+newPageSize+" controller = +searchBasedNavigation_controller");

			wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": "0","resultType":"products","pageSize":newPageSize});
			this.updateHistory();
			MessageHelper.hideAndClearMessage();
		},
		
		sortResults:function(orderBy){
			if(!submitRequest()){
				return;
			}
			cursor_wait();  
			console.debug("orderBy = "+orderBy+" controller = +searchBasedNavigation_controller");
			//Reset beginIndex = 1

			wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": "0","orderBy":orderBy,"resultType":"products"});
			this.updateHistory();
			MessageHelper.hideAndClearMessage();
		},

		sortResults_content:function(orderBy){
			if(!submitRequest()){
				return;
			}
			cursor_wait();  
			console.debug("orderBy = "+orderBy+" controller = +searchBasedNavigation_controller");
			//Reset beginIndex = 1
			wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": "0","orderByContent":orderBy,"resultType":"content"});
			this.updateHistory();
			MessageHelper.hideAndClearMessage();
		},

		swatchImageClicked:function(id) {
			// This is a workaround for IE's bug for non-clickable label images.
			var e = byId(id);
			if(!e.checked) {
				e.click();
			}
		},

		clone:function(masterObj) {
			if (null == masterObj || "object" != typeof masterObj) return masterObj;
			var clone = masterObj.constructor();
			for (var attr in masterObj) {
				if (masterObj.hasOwnProperty(attr)) clone[attr] = masterObj[attr];
			}
			return clone;
		},

		updateHistory:function() {
/*
			var contextValues = "";
			for(var i = 0; i < facetArray[2].length; i++) {
				console.debug(facetArray[2][i]);
				contextValues= contextValues + facetArray[2][i] + "|";
			}
			*/

			var currentContextProperties = wc.render.getContextById('searchBasedNavigation_context').properties;

			var contextValues = "facet:" + currentContextProperties["facetId"] + this.contextValueSeparator;
			contextValues+= "productBeginIndex:" + currentContextProperties["beginIndex"] + this.contextValueSeparator;
			contextValues+= "orderBy:" + currentContextProperties["orderBy"] + this.contextValueSeparator;
			contextValues+= "pageView:" + currentContextProperties["pageView"] + this.contextValueSeparator;
			contextValues+= "minPrice:" + currentContextProperties["minPrice"] + this.contextValueSeparator;
			contextValues+= "maxPrice:" + currentContextProperties["maxPrice"] + this.contextValueSeparator;

			var yScroll=document.body.scrollTop;
			if(history.pushState) {
				history.pushState(null, null, "#" + contextValues);
			}
			else {
				window.location.hash = contextValues;
			}
			document.body.scrollTop=yScroll;
		},

		restoreHistoryContext:function() {
			if(location.hash != null && location.hash != "" && location.hash != "#") {
				this.clearAllFacets(false);

				var productBeginIndex = "";
				var orderBy = "";
				var pageView = "";
				var minPrice = "";
				var maxPrice = "";
						
				var pairs = location.hash.substring(1).split(this.contextValueSeparator);
				for(var k = 0; k < pairs.length; k++) {
					var pair = pairs[k].split(":");
					if(pair[0] == "facet") {
						var ids = pair[1].split(",");
						for(var i = 0; i < ids.length; i++) {
							var e = byId(ids[i]);
							if (e) {
								e.checked = true;
								this.appendFilterFacet(ids[i]);
							}
						}
					}
					else if(pair[0] == "productBeginIndex") {
						productBeginIndex = pair[1];
					}
					else if(pair[0] == "orderBy") {
						orderBy = pair[1];
					}
					else if(pair[0] == "pageView") {
						pageView = pair[1];
					}
					else if(pair[0] == "minPrice") {
						byId("low_price_input").value = pair[1];
						minPrice = pair[1];
					}
					else if(pair[0] == "maxPrice") {
						byId("high_price_input").value = pair[1];
						maxPrice = pair[1];
					}
				}
				if(!submitRequest()){
					return;
				}
				cursor_wait();  

				if(minPrice != "" && maxPrice != "") {
					this.appendFilterPriceRange();
				}

				var facetArray = this.getEnabledProductFacets();
				
				wc.render.updateContext('searchBasedNavigation_context', {"productBeginIndex": productBeginIndex, "orderBy": orderBy, "pageView": pageView, "facet": facetArray[0], "facetLimit": facetArray[1], "facetId": facetArray[2], "minPrice": minPrice, "maxPrice": maxPrice});
			}
		}
	};
}

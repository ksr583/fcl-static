## Header Notes

- The logo is inside .left_espot
- `#wrapper` is only for the header, not the entire site.
- The main difference between homepage and inside page headers is that, on inside pages, the `#allDepartmentsMenu` div does not have the `.active` class by default.



## Top-Level Category Page

- Removed the row containing the marketing messages (just below breadcrumbs)
- By default, the grid of subcats shown will resize based on how many exist, just like the carousel.  The widget is actually a carousel by default.
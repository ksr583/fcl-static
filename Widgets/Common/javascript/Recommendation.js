//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2013 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

if(typeof(RecommendationJS) == "undefined" || RecommendationJS == null || !RecommendationJS){
	
	RecommendationJS = { 
	
	/**
	 * This function is used to display the next set of recommendations
	 * @param {string} widgetId The identifier of the widget
	 * @param {string} pageNumber The current page the widget is displaying
	 * @param {string} pageNumber The suffix appended to the widget
	 */		
	showNextResults: function(widgetId,pageNumber,widgetSuffix){
			if(!submitRequest()){
				return;
			}
			cursor_wait(); 
			
			var activeId = widgetId+"_"+pageNumber;
			if(widgetSuffix != undefined && widgetSuffix != ''){
				activeId += widgetSuffix;
			}	
			if(null != dojo.byId(activeId)){
				dojo.byId(activeId).style.display = "none";
				
			}
			
			pageNumber++;
			
			var nextId = widgetId+"_"+pageNumber;
			if(widgetSuffix != undefined && widgetSuffix != ''){
				nextId += widgetSuffix;
			}			
			if(null != dojo.byId(nextId)){
				dojo.byId(nextId).style.display = "block";
				
			}
			
			var currentIdValue = currentId;
			
			cursor_clear();
			
			var currentPagingButton = dojo.query("#"+nextId+" a")[0]; 
			if(!!currentPagingButton){
				currentPagingButton.focus();
			}
	},
	
	/**
	 * This function is used to display the previous set of recommendations
	 * @param {string} widgetId The identifier of the widget
	 * @param {string} pageNumber The current page the widget is displaying
	 * @param {string} pageNumber The suffix appended to the widget	 
	 */	
	 
	showPrevResults: function(widgetId, pageNumber,widgetSuffix){
			if(!submitRequest()){
				return;
			}
			cursor_wait(); 
			
			var activeId = widgetId+"_"+pageNumber;
			if(widgetSuffix != undefined && widgetSuffix != ''){
				activeId += widgetSuffix;
			}			
			if(null != dojo.byId(activeId)){
				dojo.byId(activeId).style.display = "none";
				
			}
			
			pageNumber--;
			
			var prevId = widgetId+"_"+pageNumber;
			if(widgetSuffix != undefined && widgetSuffix != ''){
				prevId += widgetSuffix;
			}			
			if(null != dojo.byId(prevId)){
				dojo.byId(prevId).style.display = "block";
				
			}
			
			var currentIdValue = currentId;
			
			cursor_clear();

			var currentPagingButton = dojo.query("#"+prevId+" a")[0]; 
			if(!!currentPagingButton){
				currentPagingButton.focus();
			}
	},
	
	showPageResults: function(widgetId, currentpageNumber, widgetSuffix, toPageNumber){
		if(!submitRequest()){
			return;
		}
		cursor_wait(); 
		
		var activeId = widgetId+"_"+ currentpageNumber;
		if(widgetSuffix != undefined && widgetSuffix != ''){
			activeId += widgetSuffix;
		}
		
		if(null != dojo.byId(activeId)){
			dojo.byId(activeId).style.display = "none";
			
		}
				
		var targetId = widgetId+"_"+ toPageNumber;
		if(widgetSuffix != undefined && widgetSuffix != ''){
			targetId += widgetSuffix;
		}			
		if(null != dojo.byId(targetId)){
			dojo.byId(targetId).style.display = "block";
			
		}
		
		var currentIdValue = currentId;
		
		cursor_clear();

		var currentPagingButton = dojo.query("#"+targetId+" a")[0]; 
		if(!!currentPagingButton){
			currentPagingButton.focus();
		}
	}
 };	
}
